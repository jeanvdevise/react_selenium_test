import './App.css';
import React from 'react';

function App() {

  const alertSuccess = () => {
    return alert('Success')
  }
  return (
    <div className="App">
      <header className="App-header">
        <button onClick={alertSuccess}>Submit</button>
      </header>
    </div>
  );
}

export default App;
